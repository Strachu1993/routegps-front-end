import { PathRecorderSummaryComponent } from './components/modals/path-recorder-summary/path-recorder-summary.component';
import { PathRegistratorComponent } from './components/path-registrator/path-registrator/path-registrator.component';
import { MenuBarComponent } from './components/menu-bar/menu-bar/menu-bar.component';
import { RouteDetailsMapComponent } from './components/route/route-details-map/route-details-map.component';
import { RouteDetailsComponent } from './components/route/route-details/route-details.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { FavoriteComponent } from './components/favorite/favorite/favorite.component';
import { TableFavoriteComponent } from './components/favorite/table-favorite/table-favorite.component';
import { MapFavoriteComponent } from './components/favorite/map-favorite/map-favorite.component';
import { AddFavoriteComponent } from './components/favorite/add-favorite/add-favorite.component';
import { RouteComponent } from './components/route/route/route.component';
import { AppRoutingModule } from './app.routing.module';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { TableRouteCatalogComponent } from './components/route/table-route-catalog/table-route-catalog.component';
import { DateDtoPipe } from './pipes/date-dto.pipe';
import { RouteDetailsTableComponent } from './components/route/route-details-table/route-details-table.component';
import { NavigatorService } from './services/navigator.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { CountingDownComponent } from './components/modals/counting-down/counting-down.component';

@NgModule({
  declarations: [
    AppComponent,
    FavoriteComponent,
    TableFavoriteComponent,
    MapFavoriteComponent,
    AddFavoriteComponent,
    RouteComponent,
    PageNotFoundComponent,
    RouteDetailsComponent,
    TableRouteCatalogComponent,
    DateDtoPipe,
    RouteDetailsMapComponent,
    RouteDetailsTableComponent,
    MenuBarComponent,
    PathRegistratorComponent,
    CountingDownComponent,
    PathRecorderSummaryComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    NavigatorService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
