import { Observable } from 'rxjs/Observable';
import { Injectable, OnInit } from '@angular/core';
import { Point } from '../interfaces/point';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

declare var navigator;
declare var position;

@Injectable()
export class NavigatorService {

  private actualPosition: Point = {
    x: 15.4501303,
    y: 51.5565064
  };
  private delay = 5000;
  private shareActualPosition = new BehaviorSubject<Point>(this.actualPosition);

  constructor() {
    this.readPositons();
  }

  getActualPosition(): Observable<Point> {
    return this.shareActualPosition.asObservable();
  }

  private readPositons() {
    // tslint:disable-next-line:no-shadowed-variable
    navigator.geolocation.getCurrentPosition(position => {
      this.actualPosition = {
        x: position.coords.longitude,
        y: position.coords.latitude
      };
    }, this.errorHandler(Error));

    this.shareActualPosition.next(this.actualPosition);
    setTimeout(() => this.readPositons(), this.delay);
  }

private errorHandler(error) {
    let errorMessage: string;

    switch (error.code) {
      case error.PERMISSION_DENIED:
      errorMessage = 'Brak zgody';
        break;
      case error.TIMEOUT:
      errorMessage = 'Minął czas oczekiwania';
        break;
      case error.POSITION_UNVAILABLE:
      errorMessage = 'Dane są niedostępne';
        break;
      case error.UNKNOW_ERROR:
        errorMessage = 'Błąd nieznany';
        break;
    }
  }

}
