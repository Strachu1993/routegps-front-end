import { Observable } from 'rxjs/Observable';
import { Injectable, OnInit } from '@angular/core';
import { RouteRestService } from './route-rest.service';
import { RouteCatalog } from '../interfaces/route-catalog';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Route } from '../interfaces/route';
import { GpsData } from '../interfaces/gps-data';
import { DateDto } from '../interfaces/my-date';
import { GpsDataText } from '../interfaces/gps-data-text';

@Injectable()
export class RouteManagementService implements OnInit {

  private routeCatalogList: Array<RouteCatalog>;
  private shareRouteCatalogList = new BehaviorSubject<Array<RouteCatalog>>(this.routeCatalogList);

  private routeList: Array<Route>;
  private shareRouteList = new BehaviorSubject<Array<Route>>(this.routeList);

  private gpsDataList: Array<GpsData>;
  private shareGpsData = new BehaviorSubject<Array<GpsData>>(this.gpsDataList);

  constructor(private routeRestService: RouteRestService) { }

  ngOnInit() {
    this.getRouteCatalogs();
    this.getRouteList();
  }

  getRouteCatalogList(): Observable<Array<RouteCatalog>> {
    return this.shareRouteCatalogList.asObservable();
  }

  getRouteList(): Observable<Array<Route>> {
    return this.shareRouteList.asObservable();
  }

  getGpsDataList(): Observable<Array<GpsData>> {
    return this.shareGpsData.asObservable();
  }

  getRouteCatalogs() {
    this.routeRestService.getRouteCatalogs().subscribe(routeCatalogs => {
      this.routeCatalogList = routeCatalogs;
      this.shareRouteCatalogList.next(this.routeCatalogList);
    });
  }

  getRoutes(routeCatalogName: string) {
    const param = new HttpParams().set('routeCatalogName', routeCatalogName);
    this.routeRestService.getRoute(param).subscribe(routes => {
      this.routeList = routes;
      this.shareRouteList.next(this.routeList);
    });
  }

  getGpsDates(routeCatalogName: string, dateBegin: DateDto) {
    const param = new HttpParams().set('routeCatalogName', routeCatalogName);
    this.routeRestService.getGpsData(param, dateBegin).subscribe(gpsList => {
      this.gpsDataList = gpsList;
      this.shareGpsData.next(this.gpsDataList);
    },
      (error: HttpErrorResponse) => {
        console.log(error.message);
        this.gpsDataList = [];
        this.shareGpsData.next(this.gpsDataList);
      }
    );
  }

  editRouteCatalog(oldName: string, newRouteCatalog: RouteCatalog) {
    const param = new HttpParams().set('name', oldName);
    this.routeRestService.editRouteCatalog(param, newRouteCatalog).subscribe(() => {
      this.getRouteCatalogs();
    });
  }

  deleteRouteCatalog(name: string) {
    const param = new HttpParams().set('name', name);
    this.routeRestService.deleteRouteCatalog(param).subscribe(() => {
      this.getRouteCatalogs();
    });
  }

  addRouteAndGpsData(routeCatalogName: string, gpsDataList: Array<GpsDataText>) {
    const param = new HttpParams().set('routeCatalogName', routeCatalogName);
    this.routeRestService.addRouteAndGpsData(param, gpsDataList).subscribe(() => {
      alert('poszlo');
    });
  }

}
