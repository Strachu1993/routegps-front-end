import { HttpParams, HttpErrorResponse } from '@angular/common/http';
import { FavoriteRestService } from './favorite-rest.service';
import { Favorite } from './../interfaces/favorite';
import { Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Point } from '../interfaces/point';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class FavoriteManagementService implements OnInit {

  favoriteList: Array<Favorite>;
  private shareFavoriteList = new BehaviorSubject<Array<Favorite>>(this.favoriteList);

  constructor(private favoriteService: FavoriteRestService) {}

  ngOnInit() {
    this.getFavorites();
  }

  getFavoriteList(): Observable<Array<Favorite>> {
    return this.shareFavoriteList.asObservable();
  }

  getFavorites() {
    this.favoriteService.getFavorites().subscribe(favorites => {
      this.favoriteList = favorites;
      this.shareFavoriteList.next(this.favoriteList);
    });
  }

  addNewFavorite(newFavoriteName: string, actualPosition: Point) {
    const favorite: Favorite = {
      name: newFavoriteName,
      points: actualPosition
    };

    this.favoriteService.addFavorite(favorite).subscribe(mes => {
      this.getFavorites();
    });
  }

  editFavorite(oldName: string, newName: string) {
    const param = new HttpParams()
      .set('oldName', oldName)
      .set('newName', newName);

    this.favoriteService.editFavorite(param).subscribe(mes => {
      this.getFavorites();
    });
  }

  deleteFavoite(event) {
    const param = new HttpParams().set('name', event);

    this.favoriteService.deleteFavorite(param).subscribe(mes => {
      this.getFavorites();
    });
  }
}
