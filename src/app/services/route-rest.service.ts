import { DateDto } from './../interfaces/my-date';
import { Route } from './../interfaces/route';
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams } from '@angular/common/http';
import { RouteCatalog } from '../interfaces/route-catalog';
import { Observable } from 'rxjs/Observable';
import { GpsData } from '../interfaces/gps-data';
import { AppSettings } from '../config/AppSettings';
import { GpsDataText } from '../interfaces/gps-data-text';

@Injectable()
export class RouteRestService {

  private urlRouteCatalog = AppSettings.SERVER_ADDRESS + 'route/catalog/';
  private urlRoute = AppSettings.SERVER_ADDRESS + 'route/';

  constructor(private http: HttpClient) { }

  getRouteCatalogs(): Observable<Array<RouteCatalog>> {
    return this.http.get<Array<RouteCatalog>>(this.urlRouteCatalog + 'get');
  }

  editRouteCatalog(param: HttpParams, newRouteCatalog: RouteCatalog): Observable<any> {
    return this.http.put<any>(this.urlRouteCatalog + 'edit', newRouteCatalog, { params: param });
  }

  deleteRouteCatalog(param: HttpParams): Observable<any> {
    return this.http.delete<any>(this.urlRouteCatalog + 'remove', { params : param });
  }

  getRoute(param: HttpParams): Observable<Array<Route>> {
    return this.http.get<Array<Route>>(this.urlRoute + 'get', { params : param });
  }

  getGpsData(param: HttpParams, dateBegin: DateDto): Observable<Array<GpsData>> {
    return this.http.post<Array<GpsData>>(this.urlRoute + 'getGpsData', dateBegin, { params: param });
  }

  addRouteAndGpsData(param: HttpParams, gpsDataList: Array<GpsDataText>): Observable<any> {
    return this.http.post<any>(this.urlRoute + 'addRouteAndGpsData', gpsDataList, { params : param });
  }

}
