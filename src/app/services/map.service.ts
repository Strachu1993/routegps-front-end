import { Injectable, ElementRef } from '@angular/core';

import OlMap from 'ol/map';
import OlXYZ from 'ol/source/xyz';
import OlTileLayer from 'ol/layer/tile';
import OlView from 'ol/view';
import OlProj from 'ol/proj';
import Style from 'ol/style/style';
import Circle from 'ol/style/circle';
import Fill from 'ol/style/fill';
import Stroke from 'ol/style/stroke';
import OlPointGeo from 'ol/geom/point';
import OlFeature from 'ol/Feature';
import OlLayerVector from 'ol/layer/Vector';
import OlSourceVector from 'ol/source/Vector';
import control from 'ol/control';
import { Point } from '../interfaces/point';

@Injectable()
export class MapService {

  private map: OlMap;
  private source: OlXYZ;
  private layer: OlTileLayer;
  private view: OlView;
  private icons = [];
  private pointLayer;
  private favoriteIconStyle = this.createIconStyle(6, [255, 0, 0, 1], 4.4, [0, 0, 0, 1]);
  private actualIconStyle = this.createIconStyle(6, [0, 0, 0, 1], 4.4, [0, 0, 208, 1]);

  constructor() { }

  addPointLayer(actualPosition, list: Array<any>) {
    this.removePoint();
    this.crateLayer(actualPosition, list);
    this.map.addLayer(this.pointLayer);
  }

  removePoint() {
    this.icons = [];
    this.map.removeLayer(this.pointLayer);
  }

  initMap(actualPosition: Point, mapId: string) {
    this.source = new OlXYZ({
      url:
        'https://api.tiles.mapbox.com/v4/mapbox.light/{z}/{x}/{y}.png?access_token=pk.eyJ1Ijoib' +
        'WFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw'
    });

    this.layer = new OlTileLayer({
      source: this.source
    });

    this.view = new OlView({
      center: OlProj.fromLonLat([actualPosition.x, actualPosition.y]),
      zoom: 15
    });

    this.map = new OlMap({
      target: mapId,
      layers: [this.layer],
      loadTilesWhileAnimating: true,
      view: this.view,
      controls: control.defaults({
        zoom: false,
        attribution: false,
        rotate: false,
      })
    });
  }

  goTo(location: Point, done) {
    const duration = 2000;
    this.view.animate({
      center: OlProj.fromLonLat([location.x, location.y]),
      duration: duration
    });
  }

  flyTo(location: Point, done) {
    const duration = 2000;
    const zoom = this.view.getZoom();
    let parts = 2;
    let called = false;

    function callback(complete) {
      --parts;
      if (called) {
        return;
      }
      if (parts === 0 || !complete) {
        called = true;
        done(complete);
      }
    }

    this.view.animate(
      {
        center: OlProj.fromLonLat([location.x, location.y]),
        duration: duration
      },
      callback
    );

    this.view.animate(
      {
        zoom: zoom - 1,
        duration: duration / 2
      },
      {
        zoom: zoom,
        duration: duration / 2
      },
      callback
    );
  }

  private createIconStyle(radiusFill: number, colorFill, widthStroke: number, colorStroke): Style {
    return new Style({
            image: new Circle({
              radius: radiusFill,
              fill: new Fill({
                color: colorFill
              }),
              stroke: new Stroke({
                color: colorStroke,
                width: widthStroke
              })
            }),
              zIndex: 1
          });
  }

  private iconCreate(x, y, icon) {
    const iconFeature = new OlFeature({
      geometry: new OlPointGeo(
        OlProj.transform([x, y], 'EPSG:4326', 'EPSG:3857')
      )
    });

    iconFeature.setStyle(icon);
    this.icons.push(iconFeature);
  }

  private crateLayer(actualPosition, list) {
    this.setIconLayer(actualPosition, list);
    this.pointLayer = new OlLayerVector({
      source: new OlSourceVector({ features: this.icons })
    });
  }

  private setIconLayer(actualPosition, list) {
    this.iconCreate(actualPosition.x, actualPosition.y, this.actualIconStyle);
    let favorite;
    for (favorite of list) {
      this.iconCreate(favorite.points.x, favorite.points.y, this.favoriteIconStyle);
    }
  }

}
