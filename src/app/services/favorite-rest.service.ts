import { Injectable } from '@angular/core';
import { Favorite } from './../interfaces/favorite';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpRequest, HttpParams } from '@angular/common/http';
import { AppSettings } from '../config/AppSettings';

@Injectable()
export class FavoriteRestService {

  private urlFavorite = AppSettings.SERVER_ADDRESS + 'favorite/';

  constructor(private http: HttpClient) {}

  getFavorites(): Observable<Array<Favorite>> {
    return this.http.get<Array<Favorite>>(this.urlFavorite + 'get');
  }

  addFavorite(body: Favorite): Observable<any> {
    return this.http.post<any>(this.urlFavorite + 'add', body);
  }

  editFavorite(param: HttpParams): Observable<any> {
    return this.http.put<any>(this.urlFavorite + 'editName', null, { params: param });
  }

  deleteFavorite(param: HttpParams): Observable<any> {
    return this.http.delete<any>(this.urlFavorite + 'remove', { params : param });
  }

}
