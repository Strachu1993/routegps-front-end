import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-path-recorder-summary',
  templateUrl: './path-recorder-summary.component.html',
  styleUrls: ['./path-recorder-summary.component.css']
})
export class PathRecorderSummaryComponent {

  @Output()
  private eventName = new EventEmitter<string>();

  sendGpsDataToServer(routeName: HTMLInputElement) {
    this.eventName.emit(routeName.value);
  }

}
