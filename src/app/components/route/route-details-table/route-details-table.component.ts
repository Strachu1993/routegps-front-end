import { Route } from './../../../interfaces/route';
import { Component, OnInit } from '@angular/core';
import { RouteCatalog } from '../../../interfaces/route-catalog';
import { ActivatedRoute, Params } from '@angular/router';
import { RouteManagementService } from '../../../services/route-management.service';

@Component({
  selector: 'app-route-details-table',
  templateUrl: './route-details-table.component.html',
  styleUrls: ['./route-details-table.component.css']
})
export class RouteDetailsTableComponent implements OnInit {

  routeCatalogName: string;
  routeList: Array<Route>;

  constructor(private route: ActivatedRoute, private routeManagementService: RouteManagementService) { }

  ngOnInit() {
    this.getRouteCatalogNameFromRoute();
    this.subscribeRouteList();
    this.routeManagementService.getRoutes(this.routeCatalogName);
  }

  private getRouteCatalogNameFromRoute() {
    this.route.paramMap.subscribe((param: Params) => {
      this.routeCatalogName = param.get('routeCatalogName');
    });
  }

  private subscribeRouteList() {
    this.routeManagementService.getRouteList().subscribe(routes => {
      this.routeList = routes;
    });
  }

}
