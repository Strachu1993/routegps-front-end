import { RouteCatalog } from './../../../interfaces/route-catalog';
import { Component, OnInit } from '@angular/core';
import { RouteManagementService } from '../../../services/route-management.service';

@Component({
  selector: 'app-table-route-catalog',
  templateUrl: './table-route-catalog.component.html',
  styleUrls: ['./table-route-catalog.component.css']
})
export class TableRouteCatalogComponent implements OnInit {

  routeCatalogList: Array<RouteCatalog>;
  editData: RouteCatalog = ({
    name: '',
    note: ''
  });

  constructor(private routeManagementService: RouteManagementService) { }

  ngOnInit() {
    this.subscribeRouteCatalogList();
  }

  editRouteCatalog(newName: HTMLInputElement, newNote: HTMLInputElement) {
    const editRouteCatalog: RouteCatalog = ({
      name: newName.value,
      note: newNote.value
    });
    this.routeManagementService.editRouteCatalog(this.editData.name, editRouteCatalog);
  }

  deleteRouteCatalog(nameRouteCatalog) {
    this.routeManagementService.deleteRouteCatalog(nameRouteCatalog);
  }

  private subscribeRouteCatalogList() {
    this.routeManagementService.getRouteCatalogList().subscribe(routeCatalogs => {
      this.routeCatalogList = routeCatalogs;
    });
  }

}
