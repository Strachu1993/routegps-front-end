import { RouteRestService } from './../../../services/route-rest.service';
import { Component, OnInit } from '@angular/core';
import { RouteManagementService } from '../../../services/route-management.service';

@Component({
  selector: 'app-route',
  templateUrl: './route.component.html',
  styleUrls: ['./route.component.css'],
  providers: [
    RouteRestService,
    RouteManagementService
  ]
})
export class RouteComponent implements OnInit {

  constructor(private routeManagementService: RouteManagementService) { }

  ngOnInit() {
    this.routeManagementService.getRouteCatalogs();
  }

}
