import { Point } from './../../../interfaces/point';
import { GpsData } from './../../../interfaces/gps-data';
import { RouteManagementService } from './../../../services/route-management.service';
import { DateDto } from './../../../interfaces/my-date';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { MapService } from '../../../services/map.service';
import { NavigatorService } from '../../../services/navigator.service';

@Component({
  selector: 'app-route-details-map',
  templateUrl: './route-details-map.component.html',
  styleUrls: ['./route-details-map.component.css'],
  providers: [MapService]
})
export class RouteDetailsMapComponent implements OnInit {

  gpsDataList: Array<GpsData>;
  routeCatalogName: string;
  dateBegin: DateDto;

  actualPosition: Point = {
    x: 15.4540763,
    y: 51.558312
  };

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private routeManagementService: RouteManagementService,
    private mapService: MapService,
    private navigatorService: NavigatorService
  ) {}

  ngOnInit() {
    this.getActualPositons();
    this.getRouteParams();
    this.subscribeGpsDataList();
    this.getGpsData();
    this.mapService.initMap(this.actualPosition , 'routeMap');
  }

  centerMapByGpsData() {
    this.mapService.flyTo(this.actualPosition, () => {});
  }

  removePoint() {
    this.mapService.removePoint();
  }

  addPointLayer() {
    this.mapService.addPointLayer(this.actualPosition, this.gpsDataList);
  }

  getGpsData() {
    this.routeManagementService.getGpsDates(this.routeCatalogName, this.dateBegin);
  }

  backToPreviousPage() {
    this.location.back();
    this.gpsDataList = null;
  }

  private getRouteParams() {
    this.route.paramMap.subscribe((param: Params) => {
      this.routeCatalogName = param.get('routeCatalogName');
      this.dateBegin = ({
        date: param.get('date'),
        time: param.get('time')
      });
    });
  }

  private subscribeGpsDataList() {
    this.routeManagementService.getGpsDataList().subscribe(gpsList => {
      this.gpsDataList = gpsList;
    });
  }

  private getActualPositons() {
    this.navigatorService.getActualPosition().subscribe((position: Point) => {
      this.actualPosition = position;
    });
  }

}
