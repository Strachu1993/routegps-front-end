import { Component, OnInit, Input } from '@angular/core';
import { FavoriteManagementService } from '../../../services/favorite-management.service';
import { Point } from '../../../interfaces/point';
import { NavigatorService } from './../../../services/navigator.service';

@Component({
  selector: 'app-add-favorite',
  templateUrl: './add-favorite.component.html',
  styleUrls: ['./add-favorite.component.css']
})
export class AddFavoriteComponent implements OnInit {

  actualPosition: Point;

  constructor(private favoriteManagementService: FavoriteManagementService, private navigatorService: NavigatorService) {}

  ngOnInit() {
    this.getActualPositons();
  }

  addNewFavorite(newFavoriteName: HTMLInputElement) {
    this.favoriteManagementService.addNewFavorite(newFavoriteName.value, this.actualPosition);
  }

  private getActualPositons() {
    this.navigatorService.getActualPosition().subscribe((position: Point) => {
      this.actualPosition = position;
    });
  }

}
