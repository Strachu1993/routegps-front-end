import { NavigatorService } from './../../../services/navigator.service';
import { HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Point } from '../../../interfaces/point';
import { Favorite } from '../../../interfaces/favorite';
import { Component, OnInit } from '@angular/core';
import { FavoriteRestService } from '../../../services/favorite-rest.service';
import { FavoriteManagementService } from '../../../services/favorite-management.service';

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.css'],
  providers: [
    FavoriteManagementService,
    FavoriteRestService
  ]
})
export class FavoriteComponent implements OnInit {

  actualPosition: Point;

  constructor(private favoriteManagementService: FavoriteManagementService, private navigatorService: NavigatorService) {}

  ngOnInit() {
    this.getActualPositons();
    this.favoriteManagementService.getFavorites();
  }

  private getActualPositons() {
    this.navigatorService.getActualPosition().subscribe((position: Point) => {
      this.actualPosition = position;
    });
  }

}
