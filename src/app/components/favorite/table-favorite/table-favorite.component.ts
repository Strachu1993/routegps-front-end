import { Favorite } from '../../../interfaces/favorite';
import { Component, EventEmitter, OnInit } from '@angular/core';
import { FavoriteManagementService } from '../../../services/favorite-management.service';

@Component({
  selector: 'app-table-favorite',
  templateUrl: './table-favorite.component.html',
  styleUrls: ['./table-favorite.component.css']
})
export class TableFavoriteComponent implements OnInit {

  editParam: string;
  favoriteList: Array<Favorite>;

  constructor(private favoriteManagementService: FavoriteManagementService) { }

  ngOnInit() {
    this.subscribeFavoriteList();
  }

  private subscribeFavoriteList() {
    this.favoriteManagementService.getFavoriteList().subscribe(favoriteList => {
      this.favoriteList = favoriteList;
    });
  }

  deleteFavoite(favoriteNameToDelete: string) {
    this.favoriteManagementService.deleteFavoite(favoriteNameToDelete);
  }

  editFavoite(newFavoriteName: HTMLInputElement) {
    this.favoriteManagementService.editFavorite(this.editParam, newFavoriteName.value);
  }

}
