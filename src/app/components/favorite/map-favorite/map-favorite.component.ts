import { Point } from '../../../interfaces/point';
import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { Favorite } from '../../../interfaces/favorite';
import { FavoriteManagementService } from '../../../services/favorite-management.service';
import { MapService } from '../../../services/map.service';
import { NavigatorService } from '../../../services/navigator.service';

@Component({
  selector: 'app-map-favorite',
  templateUrl: './map-favorite.component.html',
  styleUrls: ['./map-favorite.component.css'],
  providers: [MapService]
})
export class MapFavoriteComponent implements OnInit {

  favoriteList: Array<Favorite>;
  selectedFavorite = null;
  actualPosition: Point;

  constructor(
    private favoriteManagementService: FavoriteManagementService,
    private mapService: MapService,
    private navigatorService: NavigatorService
  ) {}

  ngOnInit() {
    this.getActualPositons();
    this.subscribeFavoriteList();
    this.mapService.initMap(this.actualPosition, 'favoriteMap');
  }

  removePoint() {
    this.mapService.removePoint();
  }

  centerMapByGpsData() {
    this.mapService.flyTo(this.actualPosition, () => {});
  }

  flyToFavorite() {
    if (null != this.selectedFavorite) {
      this.mapService.flyTo(this.selectedFavorite.points, () => {});
    }
  }

  addPointLayer() {
    this.mapService.addPointLayer(this.actualPosition, this.favoriteList);
  }

  private getActualPositons() {
    this.navigatorService.getActualPosition().subscribe((position: Point) => {
      this.actualPosition = position;
    });
  }

  private subscribeFavoriteList() {
    this.favoriteManagementService.getFavoriteList().subscribe(favoriteList => {
      this.favoriteList = favoriteList;
    });
  }

}
