import { RouteRestService } from './../../../services/route-rest.service';
import { DateDto } from './../../../interfaces/my-date';
import { GpsData } from './../../../interfaces/gps-data';
import { Point } from './../../../interfaces/point';
import { NavigatorService } from './../../../services/navigator.service';
import { Component, OnInit } from '@angular/core';
import { RouteManagementService } from '../../../services/route-management.service';
import { GpsDataText } from '../../../interfaces/gps-data-text';

declare let $;

@Component({
  selector: 'app-path-registrator',
  templateUrl: './path-registrator.component.html',
  styleUrls: ['./path-registrator.component.css'],
  providers: [
    RouteRestService,
    RouteManagementService
  ]
})
export class PathRegistratorComponent implements OnInit {

  private gpsDataList: Array<GpsDataText>; // any | GpsData
  private actualPosition: Point;
  private loopCounter = 5;
  private registerCounter = 30000;
  private isStart = false;

  constructor(private navigatorService: NavigatorService, private routeManagementService: RouteManagementService) { }

  ngOnInit() {
    this.getActualPositons();
  }

  getIsStart(): boolean {
    return this.isStart;
  }

  getPositons() {
    return this.actualPosition;
  }

  getGpsDataList() {
    return this.gpsDataList;
  }

  start() {
    $('#pathRegistrator').modal('show');
    this.countingDownInModal(this.loopCounter);
  }

  end() {
    this.isStart = false;
    $('#pathRegistratorEnd').modal('show');
  }

  sendGpsDataToServer(routeName: string) {
    this.routeManagementService.addRouteAndGpsData(routeName, this.gpsDataList);
  }

  private countingDownInModal(count: number) {
    this.setTextToId('Do rozpoczęcia: ' + count, 'timeToStart');
    setTimeout(() => {
      if (count !== 1) {
        this.countingDownInModal(--count);
      } else {
        this.go();
      }
    }, 1000);
  }

  private go() {
    $('#pathRegistrator').modal('hide');
    this.gpsDataList = [];
    this.isStart = true;
    this.storageGpsData();
  }

  private storageGpsData() {
    this.addPositionToList();

    setTimeout(() => {
      this.isTrueThenRepeat();
    }, this.registerCounter);
  }

  private isTrueThenRepeat() {
    if (this.isStart) {
      this.storageGpsData();
    }
  }

  private addPositionToList() {
    this.gpsDataList.push({
      points: {
        x: this.actualPosition.x,
        y: this.actualPosition.y
      },
      date: {
        date: this.getActualDate(),
        time: this.getActualTime()
      },
      speed: 0
    });
  }

  private getActualDate(): string {
    const dateNow = new Date();
    return this.setChar(dateNow.getDay()) + '-' + this.setChar(dateNow.getMonth()) + '-' + dateNow.getFullYear();
  }

  private getActualTime(): string {
    const dateNow = new Date();
    // tslint:disable-next-line:max-line-length
    return this.setChar(dateNow.getHours()) + ':' + this.setChar(dateNow.getMinutes()) + ':' + this.setChar(dateNow.getSeconds()) + '.' + this.setChar(dateNow.getMilliseconds());
  }

  private setChar(dateNow: number): string {
    return (dateNow > 9) ? dateNow.toString() : '0' + dateNow.toString();
  }

  private setTextToId(text: string, id: string) {
    $('#' + id).text(text);
  }

  private getActualPositons() {
    this.navigatorService.getActualPosition().subscribe((position: Point) => {
      this.actualPosition = position;
    });
  }

}
