import { RouteDetailsComponent } from './components/route/route-details/route-details.component';
import {Routes, RouterModule} from '@angular/router';
import { NgModule } from '@angular/core';
import { RouteComponent } from './components/route/route/route.component';
import { FavoriteComponent } from './components/favorite/favorite/favorite.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { TableRouteCatalogComponent } from './components/route/table-route-catalog/table-route-catalog.component';
import { RouteDetailsMapComponent } from './components/route/route-details-map/route-details-map.component';
import { RouteDetailsTableComponent } from './components/route/route-details-table/route-details-table.component';
import { PathRegistratorComponent } from './components/path-registrator/path-registrator/path-registrator.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/route',
    pathMatch: 'full'
  },
  {
    path: 'route',
    component: RouteComponent,
    children: [
      {
        path: '',
        component: TableRouteCatalogComponent
      },
      {
        path: ':routeCatalogName',
        component: RouteDetailsComponent,
        children: [
          {
            path: '',
            component: RouteDetailsTableComponent
          },
          {
            path: 'map/:routeCatalogName/:date/:time',
            component: RouteDetailsMapComponent
          }
        ]
      }
    ]
  },
  {
    path: 'favorite',
    component: FavoriteComponent
  },
  {
    path: 'pathRegistrator',
    component: PathRegistratorComponent
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
