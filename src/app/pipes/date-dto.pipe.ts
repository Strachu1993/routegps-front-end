import { Pipe, PipeTransform } from '@angular/core';
import { DateDto } from '../interfaces/my-date';

@Pipe({
  name: 'dateDtoPipe'
})
export class DateDtoPipe implements PipeTransform {

  transform(value: DateDto, args?: string): string {
    return value.date + ' ' + value.time;
  }

}
