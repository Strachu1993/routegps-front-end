export interface DateDtoText {
    date?: string;
    time?: string;
}
