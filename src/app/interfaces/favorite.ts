import { Point } from './point';

export interface Favorite {
    id?: number;
    points?: Point;
    name?: string;
}
