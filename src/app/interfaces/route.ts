import { DateDto } from './my-date';

export interface Route {
  dateBegin?: DateDto;
  dateEnd?: DateDto;
}
