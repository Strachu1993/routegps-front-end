import { Point } from './point';
import { DateDtoText } from './date-dto-text';

export interface GpsDataText {
  points?: Point;
  date?: DateDtoText;
  speed?: number;
}
