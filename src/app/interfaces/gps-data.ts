import { Point } from './point';
import { DateDto } from './my-date';

export interface GpsData {
  points?: Point;
  date?: DateDto;
  speed?: number;
}
