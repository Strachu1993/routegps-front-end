export interface RouteCatalog {
  name?: string;
  note?: string;
  count?: number;
}
