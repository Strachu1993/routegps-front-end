# Libraries/technologies/tools
	- Angular 4
	- Java Scrpit/TypeScrpit
	- HTML/CSS
	- Apache cordova
	- Bootstrap
	- OpenLayers
	- Npm

# Build
 
##1) Run Angular#
	- CMD ng serve
##2) Run cordova in the browser#
	- CMD > ng build --prod
	- CMD > cd ./mobile
	- CMD > cordova run 
##3) Run cordova in the android device#
	- CMD > ng build --prod
	- CMD > cd ./mobile
	- CMD > cordova run android --device

# Prepare to use in Windows ##

##1) Install npm(https://www.npmjs.com/)#
##2) Install Angular#
	- CMD > cd ./
	- Install Angular-cli: CMD > npm i -g @angular/cli
	- Install TypeScript: CMD > npm i -g typescript
	- Install OpenLayers: CMD > npm i openlayers
##3) Install Apache Cordova#
	- CMD > cd ./mobile
	- Install Cordova-cli: CMD > i -g cordova-cli
	- Install Apache Cordova: CMD > npm i -g cordova
	- Install platform android: CMD > cordova platform add android
	- Install platform browser: CMD > cordova platform add browser	
	- Install plugin cordova-plugin-geolocation: CMD > cordova plugin add cordova-plugin-geolocation
##4) Install Android#
	- Install Android Studio(https://developer.android.com/studio/index.html)
	- Install android versions > Android studio -> goto SDK Manager -> Appearance & Behavior -> System Settings -> Android SDK -> SDK Platform -> Select and install your android versions